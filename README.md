Hi and welcome to this guide for the JavaScript Testing Workshop.

---
## TOC
1. Prerequisites / Installing dependencies
2. What do we want to test?
3. Adding the first test
4. Expanding on the previous test

---

## 1. Prerequisites / Installing dependencies 💻

To follow along, I have to presume at least some familiarity with JavaScript.
This might mean that you already have a computer where you are able to run JavaScript code and tests.
In any case, now follows a quick recap what you need and how to get it.

### Barebones project with Jest added

If you would like to just `git clone` an existing project with Jest already setup, you can clone this repository https://gitlab.com/improve-software-quality/workshops/js-testing-workshop
_If you clone the project, continue with step “Add a command to run tests”_

### Installing dependencies

The following instructions help you setup everything yourself.

#### Yarn / NPM

You can use this tutorial regardless of whether you like to use `yarn` or `npm` to manage your packages and run your scripts.

If you need to install Yarn, follow the instructions provided here: https://yarnpkg.com
If you'd rather use npm, follow those instructions: https://docs.npmjs.com/downloading-and-installing-node-js-and-npm

#### Creating a new project

Create a new directory
```bash
$ mkdir -p js-testing-workshop
$ cd js-testing-workshop
$ yarn init # follow the on-screen instructions
```

You can choose any license, but MIT is usually fine.

#### Installing Jest

Install Jest using yarn:

`yarn add --dev jest`

Or npm:

`npm install --save-dev jest`

**Coach: Talk about dev dependencies and why we put Jest there**

#### Add a command to run tests

Add the following section to your package.json:

```json
"scripts": {
  "test": "jest"
}
```
**Coach: Talk about the scripts section**

No you can run the command `$ yarn test` and have your tests executed. Since you don't have any tests yet, you will get something similar to the following output.

```bash
$ yarn test                                                                                               yarn run v1.19.2
$ jest
No tests found, exiting with code 1
Run with `--passWithNoTests` to exit with code 0
In /Users/holgerfrohloff/projects/workshops/js-testing-workshop
  1 file checked.
  testMatch: **/__tests__/**/*.[jt]s?(x), **/?(*.)+(spec|test).[tj]s?(x) - 0 matches
  testPathIgnorePatterns: /node_modules/ - 1 match
  testRegex:  - 0 matches
Pattern:  - 0 matches
error Command failed with exit code 1.
info Visit https://yarnpkg.com/en/docs/cli/run for documentation about this command.
```
**Coach: Talk a bit about how Jest knows which files to test**


If you look at the output, you'll see how to run without tests and still have the test suite pass:

```bash
$ yarn test --passWithNoTests                                                                             yarn run v1.19.2
$ jest --passWithNoTests
No tests found, exiting with code 0
✨  Done in 0.51s.
```

🎉 Congratulations. This was your first successful test suite.  
  

## 2. What do we want to test? 🔎

Now it's time to decide what you want to test. There is a development methodology called Test-Driven Development (or short TDD). TDD helps you write code that is well tested. Code that is well tested tends to have fewer defects (bugs 🐛).

To develop using TDD, you write a (failing) test. Then you write the least amount of code that makes sense and makes the test pass. Then you refactor your code to improve it and to better reflect the knowledge you gained about your problem and solution domain.
This cycle is called Red-Green-Refactor. The colors come from the output of your test framework (here, Jest). A failing test is marked with red text, passing tests are marked with green text.

**Coach: Talk about TDD from your experience**

### A simple project

Now that we know a bit about our basic testing workflow, we need a project to use it on. The best project to experiment with using TDD is a simple calculator. Almost everyone knows how they work. That has the benefit that you don't need to learn anything about the problem domain and can focus on the solution domain. Our calculator should only support the four basic calculations: addition, subtraction, multiplication and division.

We start with addition.

Add a file to the `tests/` folder: `calculator.test.js`

```js
// calculator.test.js

test('add two numbers', () => {
  const result = 1 + 2
  
  expect(result).toEqual(3)
})
```

This test file shows you some of the basic syntax and building blocks for writing a (unit) test.

You have:
* a name/description (`add two numbers`)
* a “system under test” (`1 + 2`)
* an expectation/assertion (the whole `expect` line; the “system under test” should behave in a way that the result equals `3`)

**Arrange, Act, Assert**. The “three As” can help you in structuring your test.
In the above example, we do not have the “Arrange” step. This will be added later.

## 3. Adding the first test

Of course, we already have a test. But I would like to have a test specific to the small project we are creating — the calculator.

The next step consists of some smaller steps.  
Here's what we want to achieve:

* Create a JavaScript class for the calculator (where we add the implementation)
* Create a unit test to assert that addition works (**red**)
* Implement the addition function to make the test pass (**green**)
 
### Step 1: Arrange

```js
 // calculator.test.js
 import Calculator from '../src/calculator'
 
 describe('Calculator', () => {
     describe('add()', () => {
         it('sums two numbers', () => {
             // Arrange
             const calculator = new Calculator()
         })
     })
 })
```

 
### Step 2: Act

```js
 // calculator.test.js
 import Calculator from '../src/calculator'
 
 describe('Calculator', () => {
     describe('add()', () => {
         it('sums two numbers', () => {
             // Arrange
             const calculator = new Calculator()
             
             // Act
             const result = calculator.add(1, 2)
         })
     })
 })
```

 
### Step 3: Assert

```js
 // calculator.test.js
 import Calculator from '../src/calculator'
 
 describe('Calculator', () => {
     describe('add()', () => {
         it('sums two numbers', () => {
             // Arrange
             const calculator = new Calculator()
             
             // Act
             const result = calculator.add(1, 2)
             
             // Assert
             expect(result).toEqual(3)
         })
     })
 })
```

### The result

If you run this test, it will fail. Your goal is to add the most minimal implementation to make the test pass for each step.

**Coach: Try to guide the participant towards a solution using TDD.**

_Step 1_
_Example_
For “Step 1: Arrange” this could look like this:

```js
// src/calculator.js

class Calculator { }

export default Calculator
```

This makes the class importable and instantiatable for the “arrange phase”.

_Step 2_

Try to add the “Act” phase to the test and run it. See what error message you'll get in your terminal. Try to understand what the most minimal implementation would look like to make the test pass.

**Coach: Talk about what taking the smallest possible step could look like. Usually the participants try to do too much at once.**

_Step 3_
Repeat what you did in step 2.

## 4. Expanding on the previous test

Now that you have written a basic implementation for the `add()` function you can decide what you want to tackle next.

Here are a few suggestions:

* Add error handling for the `add()` function **using TDD**. (Learn goals: How to test for and expect errors)
* Add at least one other function like `subtract()` (Learn goals: Repeat and build upon what you did with `add()`. Try to keep using **TDD** even though you want to be quick.)
* Ask a coach about “code coverage” and find out how much you reached. Discuss with a partner how much code coverage makes sense in a project. 
